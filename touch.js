/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */
/* Copyright 2012 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* globals PDFView, Hammer, MAX_SCALE, MIN_SCALE */

//#include ../external/hammer.js/hammer.js

'use strict';

var PINCH_TO_ZOOM_TIMEOUT = 50;//150;
var PINCH_TO_ZOOM_SENSITIVITY = 0.3;//0.5;
var MAX_SCALE = 3;
var MIN_SCALE = 0.25;

function PinchManager() {
  var lastOp = 0;
  var startScale = 1.0;
  var newScale = -1;
  var inScale = false;
  var startPageWidth = 1.0;
  var startPageHeight = 1.0;
  var startScrollTop = 0;
  var startScrollLeft = 0;
  var counter = 0;

  var gaurdEndPinchTimer = null;

  this.initialize = function(options) {
    this.container = options.container;
    this.manager = options.manager;
  };

  this.startPinch = function() {
      inScale = true;
      this.manager.prePinch();

      startScale = this.manager.getZoomLevel();
      newScale = startScale;
      startPageWidth = this.manager.getWidth() * startScale;
      startPageHeight = this.manager.getHeight() * startScale;
      startScrollTop = this.container.scrollTop;
      startScrollLeft = this.container.scrollLeft;
              
      //console.log("startPinch -> startScale: " + startScale + "; startPageWidth: " + startPageWidth + "; startPageHeight: " + startPageHeight);

      lastOp = new Date();
  };

  this.pinch = function(scale) {
    var self = this;

    if(lastOp == 0)
      this.startPinch();

    var now = new Date();
    var diff = now - lastOp;
    if(diff < 100) {
      return;
    }

    lastOp = now;

    newScale = startScale * scale;
    var roundedScale = Math.floor(newScale * 100) / 100;
    if (roundedScale > MAX_SCALE) {
      roundedScale = MAX_SCALE;
    } else if (newScale < MIN_SCALE) {
      roundedScale = MIN_SCALE;
    }
    newScale = roundedScale;
    var canvasScale = newScale / startScale;

    var pages = this.manager.getPages();

    //var newTopDisplayed = startScrollTop * canvasScale;
    //var newBottomDisplayed = newTopDisplayed + this.container.clientWidth;

    //console.log("Pinch -> newScale: " + newScale + "; startScale: " + startScale + "; canvasScale: " + canvasScale);


    var top = 0;
    var displayedPages = "";
    for(var i = 0; i < pages.length; i++) {
      pages[i].fastScale(newScale);
    }

    this.container.scrollTop = startScrollTop * canvasScale;
    this.container.scrollLeft = startScrollLeft * canvasScale;

    if(gaurdEndPinchTimer) {
      clearTimeout(gaurdEndPinchTimer);
      gaurdEndPinchTimer = setTimeout(function() {
        if(inScale) {
          self.endPinch();

          console.log("end pinch timer called");
        }
        gaurdEndPinchTimer = null;
      }, 1000);
    }
    
    
  };

  this.endPinch = function() {
    if(!gaurdEndPinchTimer) {
      clearTimeout(gaurdEndPinchTimer);
      gaurdEndPinchTimer = null;
    }

    if(lastOp == 0) {
      inScale = false;
      return;
    }
    lastOp = 0;

    //document.getElementById("scale").innerHTML = "end scale " + newScale;
    //console.log("Post Pinch -> newScale: " + newScale);

    this.manager.postPinch(newScale);
    //this.manager.postPinch(newScale).then(function() {
      inScale = false;
    //});

    //Update scale menu bar
    var e = document.createEvent("UIEvents");
    e.initUIEvent("scalechange", !1, !1, window, 0);
    e.scale = newScale;
    e.resetAutoSettings = true;
    window.dispatchEvent(e);

    newScale = -1;
  };

  this.pinchScale = function() {
    if(newScale < 0)
      return this.manager.getZoomLevel();
    return newScale;
  };

  this.isInPinch = function() {
    return inScale;
  };
};

function PinchMouse() {
  var startX = -1;
  var startY = -1;
  var pinchManager;

  this.initialize = function(options) {
    var self = this;
    this.container = options.container;
    this.manager = options.manager;
    this.pinchManager = options.pinchManager;

    $(this.container).mousedown(function(e) {
      if(e.shiftKey) {
        startX = e.pageX;
        startY = e.pageY;
        self.pinchManager.startPinch();
        //console.log(e);
        //console.log("Start pinch");

        e.stopPropagation();
      }
    });

    $(this.container).mousemove(function(e) {
      if(e.shiftKey && startX > 0) {
        var x = e.pageX;
        var y = e.pageY;

        var dist = Math.sqrt(Math.pow((x - startX), 2) + Math.pow((y - startY), 2));
        var norm = dist / 100.0;
        var scale = 1.0 + norm * (y - startY)/Math.abs((y - startY));
        document.getElementById("scale").innerHTML = scale;

        self.pinchManager.pinch(scale);
        e.stopPropagation();
      }
    });

    $(this.container).mouseup(function(e) {
      if(startX > 0) {
        self.pinchManager.endPinch();         
        //console.log("End pinch");

        startX = -1;
        startY = -1;        
        e.stopPropagation();
      }
    });
  };
};

function PinchKeyboard() {
  var scale = -1;
  var inScale = false;

  this.initialize = function(options) {
    var self = this;
    this.container = options.container;
    this.manager = options.manager;
    this.pinchManager = options.pinchManager;

    $(document).keydown(function(e) {
      if (e.keyCode == 65 && e.ctrlKey) { //up arrow
        if(scale < 0)
          scale = self.pinchManager.pinchScale();  
        scale *= 1.1;
        self.pinchManager.pinch(scale);
        e.stopPropagation();
        inScale = true;
      } else if (e.keyCode == 66 && e.ctrlKey) { //down arrow
        if(scale < 0)
          scale = self.pinchManager.pinchScale();  
        scale *= 0.9090909090909091;
        self.pinchManager.pinch(scale);
        e.stopPropagation();
        inScale = true;
      }

      //if(e.keyCode == 40 || e.keyCode == 38)
      //  e.stopPropagation();
    });

    $(document).keyup(function(e) {
      if (inScale && !e.ctrlKey) {
        self.pinchManager.endPinch();
        inScale = false;
        scale = -1;
        e.stopPropagation();
      }      
    });
  };
};

function PinchGesture() {

  this.initialize = function(options) {
    var self = this;
    this.container = options.container;
    this.manager = options.manager;
    this.pinchManager = options.pinchManager;

    //TODO - check implementation of http://www.appliness.com/multitouch-with-hammer-js/

    //delete Hammer.defaults.stop_browser_behavior.userSelect;
    //hammer.js stops text selection - (set https://github.com/EightMedia/hammer.js/issues/50
    var listener = new Hammer(this.container, {
      //transform_always_block: true
      //transform_always_block: true,
      //prevent_default: false,
      stop_browser_behavior: false,
      transform_min_scale: 1,
      drag_block_horizontal: false,
      drag_block_vertical: false,
      drag_min_distance: 0
    });

    listener.on('touch drag transform release', function(e) {
      //document.getElementById("eventType").innerHTML = e.gesture.touches.length + " touches";
      /*if(!e.gesture || !e.gesture.touches || e.gesture.touches.length != 2) {//prevent non-zoom handling
        return;
      }*/
      //console.log("gesture -> type: " + e.type + "; phase: " + e.gesture.eventType);

      if(e.gesture.eventType === "start" && e.type === "touch") {
        if(e.gesture.touches.length == 2) {
          self.pinchManager.startPinch();
        }
      }
      if(e.gesture.eventType === "end") {
        self.pinchManager.endPinch();
      }
      if (!self.manager.isPresentationMode && e.type === 'transform') {
        self.pinchManager.pinch(e.gesture.scale);
      }
    });
  };

};


var TouchEvents = {
  initialize: function touchEventsInitialize(options) {
    var self = this;

    this.container = options.container;
    this.pinchToZoom = options.pinchToZoom;
    this.pinchToZoomTimeout = null;
    this.manager = options.manager;

    this.pageReplacers = {};

    this.pinchManager = new PinchManager();
    this.pinchManager.initialize(options);

    this.pinchMouse = new PinchGesture();//PinchGesture();//PinchKeyboard();
    this.pinchMouse.initialize({container: options.container, manager: options.manager, pinchManager: this.pinchManager});
    
    this.container.onscroll = function() {
      _.throttle(function() {viewer.scroll()}, 500)();
    }
  }
};
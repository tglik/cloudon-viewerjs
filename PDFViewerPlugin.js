/**
 * @license
 * Copyright (C) 2013 KO GmbH <copyright@kogmbh.com>
 *
 * @licstart
 * The JavaScript code in this page is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * (GNU AGPL) as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.  The code is distributed
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU AGPL for more details.
 *
 * As additional permission under GNU AGPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * As a special exception to the AGPL, any HTML file which merely makes function
 * calls to this code, and for that purpose includes it by reference shall be
 * deemed a separate work for copyright law purposes. In addition, the copyright
 * holders of this code give you permission to combine this code with free
 * software libraries that are released under the GNU LGPL. You may copy and
 * distribute such a system following the terms of the GNU AGPL for this code
 * and the LGPL for the libraries. If you modify this code, you may extend this
 * exception to your version of the code, but you are not obligated to do so.
 * If you do not wish to do so, delete this exception statement from your
 * version.
 *
 * This license applies to this entire compilation.
 * @licend
 * @source: http://viewerjs.org/
 * @source: http://github.com/kogmbh/Viewer.js
 */

'use strict';

/*jslint browser: true*/
/*jshint nomen: true */
/*global document, PDFJS, console, TextLayerBuilder, TouchEvents, $, jQuery, alert, _*/


var RENDERING = {
    BLANK: 0,
    RUNNING: 1,
    CACHED: 2,
    FINISHED: 3
};

var RENDERING_TYPE = {
    CACHED: 0,
    FULL: 1
};

var TASK_PRIORITY = {
    CACHED: function(page) {
        return 10 + page.pageInfo.pageIndex;
    },
    IMMEDIATE: function(page) {
        return 0;
    }
};

function RenderQueue() {
    this.tasks = [];
    this.currentTask = null;
}

RenderQueue.prototype = {
    log: function(msg) {
        //TODO: comment out for production
        console.log(msg);
    },
    logTask: function(task) {
        return "page=" + task.page.pageInfo.pageIndex + "; cache=" + (task.textLayer ? "no" : "yes") + "; priority=" + task.priority;
    },
    logCurrentTask: function() {
        return this.logTask(this.currentTask);
    },
    size: function() {
        return this.tasks.length;
    },
    get: function(index) {
        if (index < 0 || index > this.tasks.length) {
            return null;
        }
        return this.tasks[index];
    },
    top: function() {
        return this.get(0);
    },
    isSamePageTasks: function(t1, t2) {
        return t1.page.pageInfo.pageIndex === t2.page.pageInfo.pageIndex;
    },
    insert: function(index, task) {
        var i, t;
        if (this.currentTask && this.isSamePageTasks(this.currentTask, task)) {
            if (this.currentTask.priority < task.priority) {
                this.log("renderQ - block adding task: " + this.logCurrentTask() + " due to running task: " + this.logTask(task));
                task.cancel();
                return;
            }
        }

        for (i = this.size() - 1; i >= 0; i -= 1) {
            t = this.get(i);
            if (this.isSamePageTasks(t, task)) {
                if (t.priority < task.priority) {
                    this.log("renderQ - block adding task: " + this.logTask(task) + " due to waiting task: " + this.logTask(t));
                    
                    task.cancel();
                    return;
                }

                this.log("renderQ - remove waiting task: " + this.logTask(t) + " due to new task: " + this.logTask(task));

                this.removeAt(i);
                t.cancel();
            }
        }

        this.tasks.splice(index, 0, task);
        this.log("renderQ - adding task: " + this.logTask(task));

        this.checkPriority();
    },
    removeAt: function(index) {
        this.tasks.splice(index, 1);
    },
    remove: function(task) {
        var i;
        for (i = 0; i < this.size(); i += 1) {
            if (task === this.tasks[i]) {
                this.removeAt(i);
                return;
            }
        }
    },
    findIfTask: function(condition, task) {
        var found = true;
        if (condition.pageIndex !== undefined && task.page.pageInfo.pageIndex !== condition.pageIndex) {
            found = false;
        }
        if (found && condition.priority !== undefined && task.priority !== condition.priority) {
            found = false;
        }
        return found;
    },
    removeIf: function(condition) {
        var i, t;
        if (this.currentTask && this.findIfTask(condition, this.currentTask)) {
            this.log("renderQ - cancel run task: " + this.logCurrentTask() +
                " due to remove condition: page=" + (condition.pageIndex === undefined ? "{}" : condition.pageIndex) + "; priority=" + (condition.priority === undefined ? "{}" : condition.priority));

            this.currentTask.cancel();
            this.currentTask = null;
            this.checkPriority();
        }
        for (i = this.size() - 1; i >= 0; i -= 1) {
            t = this.get(i);
            if (this.findIfTask(condition, t)) {
            this.log("renderQ - remove task: " + this.logTask(t) +
                " due to remove condition: page=" + (condition.pageIndex === undefined ? "{}" : condition.pageIndex) + "; priority=" + (condition.priority === undefined ? "{}" : condition.priority));

                this.removeAt(i);
                t.cancel();
            }
        }
    },
    findIf: function(condition) {
        var i, t;
        if (this.currentTask && this.findIfTask(condition, this.currentTask)) {
            return true;
        }
        for (i = this.size() - 1; i >= 0; i -= 1) {
            t = this.get(i);
            if (this.findIfTask(condition, t)) {
                return true;
            }
        }

        return false;
    },
    clear: function() {
        this.tasks = [];
    },
    runTask: function() {
        if (this.size() === 0) {
            return;
        }

        var self = this;
        this.currentTask = this.pop();
        this.currentTask.render().then(function() {
            self.log("renderQ - finished task: " + self.logCurrentTask());

            self.currentTask = null;
            self.runTask();
        }, function(data) {
            console.log("failed on rendering in q: " + JSON.stringify(data, null, 4));
        });

        this.log("renderQ - running task: " + self.logCurrentTask());
    },
    checkPriority: function() {
        var prevTask = null,
            isEmptyQ = !this.currentTask && (this.size() > 0),
            hasHigherPriorityTask = this.currentTask && this.top() && this.currentTask.priority > this.top().priority;
        if (hasHigherPriorityTask) {
            this.currentTask.pause();
            prevTask = this.currentTask;

            this.log("renderQ - pausing task: " + this.logCurrentTask() + " due to task: " + this.logTask(this.top()));
        }
        if (isEmptyQ || hasHigherPriorityTask) {
            this.runTask();
        }
        if (prevTask) {
            this.push(prevTask);
        }
    },
    push: function(task) {
        var i, t;
        for (i = 0; i < this.size(); i += 1) {
            t = this.get(i);
            if (t.priority > task.priority) {
                this.insert(i, task);
                return;
            }
        }
        this.insert(this.size(), task);
    },
    pop: function() {
        return this.tasks.shift();
    }
};

var renderQueue = new RenderQueue();

function CachedCanvas(options) {
    this.scale = options.scale || 1.0;
    this.state = options.state || RENDERING.BLANK;
    this.canvas = options.canvas || null;
    this.textLayer = options.textLayer || null;
}
CachedCanvas.prototype = {
    internalRender: function(options) {
        var canvas = options.canvas,
            width = options.width || canvas.width,
            height = options.height || canvas.height,
            ctxMain = canvas.getContext('2d');

        ctxMain.drawImage(this.canvas, 0, 0, width, height);
    },
    
    render: function(options) {
        var self = this;
        //requestAnimationFrame(function() {
            self.internalRender(options);
        //});
    }
};

function PageRenderTask(options) {
    this.cancelled = false;
    this.internalTask = null;

    this.page = options.page;
    this.scale = options.scale || 1.0;
    this.stateTo = options.stateTo || RENDERING.BLANK;
    this.canvas = options.canvas || document.createElement('canvas');
    this.textLayer = options.textLayer || null;
    this.wait = options.wait || false;
    this.doubleBuffer = options.doubleBuffer || false;

    this.priority = options.priority !== undefined ? options.priority : TASK_PRIORITY.CACHED(this.page);

    this.viewport = this.page.getViewport(this.scale);

    this.readyDeferred = new jQuery.Deferred();
}

PageRenderTask.prototype = {

    setCanvasSize: function(canvas, width, height) {
        canvas.style.width = width;
        canvas.style.height = height;
        canvas.width = width;
        canvas.height = height;
        $(canvas).css("-webkit-transform", "translate3d(0,0,0)"); //hardware accelaration
    },

    internalRender: function() {

        if (this.cancelled) {
            return;
        }

        var self = this,
            viewport = self.page.getViewport(this.scale),
            canvasRender = self.canvas;
        if(this.doubleBuffer) {
            canvasRender = document.createElement('canvas');
            $(canvasRender).css("-webkit-transform", "translateZ(0)"); //hardware accelaration
        }
        self.setCanvasSize(canvasRender, viewport.width, viewport.height);
        this.state = RENDERING.RUNNING;

        this.internalTask = this.page.render({
            canvasContext: canvasRender.getContext('2d'),
            textLayer: self.textLayer,
            viewport: viewport
        });
        this.internalTask.promise.then(function() {
            if (self.cancelled) {
                return;
            }
            if(self.doubleBuffer) {
                self.setCanvasSize(self.canvas, viewport.width, viewport.height);
                var ctxMain = self.canvas.getContext('2d');
                ctxMain.drawImage(canvasRender, 0, 0, viewport.width, viewport.height);
            }

            self.readyDeferred.resolveWith(self, [new CachedCanvas({scale: self.scale,
                                                            state: self.stateTo,
                                                            canvas: self.canvas,
                                                            textLayer: self.textLayer})]);
            self.state = RENDERING.FINISHED;
        }, function(data) {
            self.readyDeferred.reject();
            this.state = RENDERING.BLANK;
            console.log("rendering rejected " + JSON.stringify(data, null, 4));
        });

        return self.readyDeferred;//this.internalTask.promise;
    },

    render: function() {
        var self = this;
        if (this.cancelled) {
            return;
        }

        if (this.wait) {
            //_.defer(_.bind(this.internalRender, this));
            setTimeout(function() {
                //requestAnimationFrame(_.bind(self.internalRender, self));
                //_.bind(self.internalRender, self)
                self.internalRender();

            }, 0);
            //_.defer(requestAnimationFrame(_.bind(this.internalRender, this)));
        } else {
            this.internalRender();
            //requestAnimationFrame(_.bind(this.internalRender, this));
        }
        
        
        return this.promise();
    },

    promise: function() {
        return this.readyDeferred.promise();
    },

    cancel: function() {
        this.cancelled = true;
        this.state = RENDERING.BLANK;

        if (this.internalTask) {
            this.internalTask.cancel();
        }

        if (this.readyDeferred.state() === "pending") {
            this.readyDeferred.reject();
        }
    },

    pause: function() {
        if (this.internalTask) {
            this.internalTask.cancel();
        }
    }
};

function PageView(options) {
    this.page = options.page;
    this.container = options.container;
    this.plugin = options.plugin;

    this.domPage = null;
    this.textLayer = null;
    this.textLayerDiv = null;
    this.pageWidth = 0;
    this.pageHeight = 0;
    this.pageNumber = 0;

    this.state = RENDERING.BLANK;

    this.renderTask = null;
    this.cacheCanvas = null;

    this.inPinch = false;
    this.inDisplay = false;
}
PageView.prototype = {

    updatePageDimensions: function(width, height, completeSize) {
        this.domPage.style.width = width;
        this.domPage.style.height = height;

        if(this.canvas) {
            this.canvas.style.width = width;
            this.canvas.style.height = height;
            if (completeSize) {
                this.canvas.width = width;
                this.canvas.height = height;
            }
        }
    },

    updatePageDimensionsScale: function(completeSize) {
        this.updatePageDimensions(this.pageWidth * this.scale, this.pageHeight * this.scale, completeSize);
    },

    createPage: function() {
        var viewport,
            self = this;

        this.pageNumber = this.page.pageInfo.pageIndex + 1;

        viewport = this.page.getViewport(this.plugin.getScale());

        this.domPage = document.createElement('div');
        this.domPage.id = 'pageContainer' + this.pageNumber;
        this.domPage.className = 'page';

        $(this.domPage).addClass("loadingIcon");

        this.canvas = document.createElement('canvas');
        this.canvas.id = 'canvas' + this.pageNumber;

        this.textLayerDiv = document.createElement('div');
        this.textLayerDiv.className = 'textLayer';
        this.textLayerDiv.id = 'textLayer' + this.pageNumber;

        this.container.appendChild(this.domPage);
        this.domPage.appendChild(this.canvas);
        this.domPage.appendChild(this.textLayerDiv);

        this.updatePageDimensions(viewport.width, viewport.height, true);
        this.pageWidth = viewport.width;
        this.pageHeight = viewport.height;

        this.textLayer = new TextLayerBuilder({
            textLayerDiv: this.textLayerDiv,
            pageIndex: this.pageNumber - 1
        });
        this.page.getTextContent().then(function (textContent) {
            self.textLayer.setTextContent(textContent);
        });
    },

    clearPage: function() {
        //$(this.textLayer.textLayerDiv).empty();
        this.textLayer.empty();
        this.state = (this.state === RENDERING.BLANK) ? RENDERING.BLANK : RENDERING.CACHED;
    },

    load: function() {
        var self = this;
/*        this._createPage();

        var cacheRenderTask = new PageRenderTask({page:this.page, 
            canvas: this.page.pageInfo.pageIndex == 0 ? this.canvas : null,
            textLayer: this.page.pageInfo.pageIndex == 0 ? this.textLayer : null,
            stateTo: this.page.pageInfo.pageIndex == 0 ? RENDERING.FINISHED : RENDERING.CACHED,
            scale: 1.0,
            wait: this.page.pageInfo.pageIndex > 0
        });*/

        self.createPage();
        var isFirstPage = self.page.pageInfo.pageIndex === 0,
            cacheRenderTask = new PageRenderTask({
                page: self.page,
                canvas:  isFirstPage ? self.canvas : null,
                textLayer: isFirstPage ? self.textLayer : null,
                stateTo: isFirstPage ? RENDERING.FINISHED : RENDERING.CACHED,
                scale: 1.0,
                wait: !isFirstPage,
                priority: isFirstPage ? TASK_PRIORITY.IMMEDIATE(self.page) : TASK_PRIORITY.CACHED(self.page)
            });

        if (self.page.pageInfo.pageIndex === 0) {
            self.renderTask = cacheRenderTask;
        }
        //console.log("loading page " + self.pageNumber);


        //var promiseWaitRender = promiseFirstPage;
        if (this.page.pageInfo.pageIndex === 0) {
            //this.renderTask = cacheRenderTask;
            this.inDisplay = true;
            //promiseWaitRender = (new jQuery.Deferred()).resolve().promise();
        }
        //var promiseRender = new jQuery.Deferred();

        //cacheRenderTask.render().then(function(cacheCanvas) {
        cacheRenderTask.promise().then(function(cacheCanvas) {
            self.renderTask = null;
            self.cacheCanvas = cacheCanvas;

            if (self.state === RENDERING.BLANK && !self.renderTask) {
                cacheCanvas.render({canvas: self.canvas});
            }
            self.state = cacheCanvas.state;

            $(self.domPage).removeClass("loadingIcon");
        });

        renderQueue.push(cacheRenderTask);
        return cacheRenderTask.promise();
    },

    fastScale: function(scale) {
        this.domPage.style.width = this.pageWidth * scale;
        this.domPage.style.height = this.pageHeight * scale;
        this.canvas.style.width = this.pageWidth * scale;
        this.canvas.style.height = this.pageHeight * scale;

        this.scale = scale;
    },

    setScale: function(scale) {
        //this.clearRendering();

        this.scale = scale;
        this.updatePageDimensions(this.pageWidth * scale, this.pageHeight * scale, false);
        this.state = RENDERING.BLANK;

        if (this.isScrolledIntoView()) {
            this.renderFull(false);
        }
    },

    isScrolledIntoView: function() {
        if (!this.domPage) {
            return false;
        }

        var docViewTop = this.container.scrollTop,
            docViewBottom = docViewTop + this.container.clientHeight,
            elemTop = this.domPage.offsetTop,
            elemBottom = elemTop + this.domPage.clientHeight;

        // Is in view if either the top or the bottom of the page is between the
        // document viewport bounds,
        // or if the top is above the viewport and the bottom is below it.
        return (elemTop >= docViewTop && elemTop < docViewBottom)
                || (elemBottom >= docViewTop && elemBottom < docViewBottom)
                || (elemTop < docViewTop && elemBottom >= docViewBottom);
    },

    scrollIntoView: function() {
        this.domPage.parentNode.scrollTop = this.domPage.offsetTop;
    },

    renderFull: function(wait) {
        var self = this;
        this.renderTask = new PageRenderTask({
            page: this.page,
            canvas: this.canvas,
            textLayer: this.textLayer,
            stateTo: RENDERING.FINISHED,
            scale: this.scale,
            wait: wait,
            doubleBuffer: true,
            priority: TASK_PRIORITY.IMMEDIATE(this.page)
        });

        this.renderTask.promise().then(function() {
            self.renderTask = null;
            self.state = RENDERING.FINISHED;
            $(self.domPage).removeClass("loadingIcon");
        });

        renderQueue.push(this.renderTask);
    },

    scrollIn: function() {
        var inDisplay = this.isScrolledIntoView();

        if (this.inDisplay !== inDisplay && !inDisplay) {
            this.clearPage();
            renderQueue.removeIf({pageIndex: this.page.pageInfo.pageIndex, priority: TASK_PRIORITY.IMMEDIATE(this.page)});
        }

        if(inDisplay && this.state !== RENDERING.FINISHED && this.state !== RENDERING.CACHED && this.cacheCanvas) {
            this.cacheCanvas.render({canvas: this.canvas});
            this.state = RENDERING.CACHED;
            //console.log("rendering cache on scrollIn")
        }

        var hasRenderingTask = renderQueue.findIf({pageIndex: this.page.pageInfo.pageIndex, priority: TASK_PRIORITY.IMMEDIATE(this.page)});
        if (!inDisplay || hasRenderingTask || this.state === RENDERING.FINISHED || this.plugin.inPinch) {
            return;
        }

        //if (this.textLayer) {
        //  this.textLayer.updateMatches();
        //}

        this.inDisplay = inDisplay;
        this.renderFull(false);
    },

    prePinch: function() {
        //this._clearRendering();

        this.clearPage();
    },

    postPinch: function() {
        if (!this.isScrolledIntoView()) {
            return;
        }
        this.renderFull(true);
    },

    getWidth: function() {
        return this.pageWidth;
    },

    getHeight: function() {
        return this.pageHeight;
    }
};

function PDFViewerPlugin() {

    function init(callback) {
        var pdfLib, textLayerLib, pluginCSS;

        pdfLib = document.createElement('script');
        pdfLib.async = false;
        pdfLib.src = './pdf.js';
        pdfLib.type = 'text/javascript';
        pdfLib.onload = function () {
            textLayerLib = document.createElement('script');
            textLayerLib.async = false;
            textLayerLib.src = './TextLayerBuilder.js';
            textLayerLib.type = 'text/javascript';
            textLayerLib.onload = callback;
            document.getElementsByTagName('head')[0].appendChild(textLayerLib);
        };
        document.getElementsByTagName('head')[0].appendChild(pdfLib);

        pluginCSS = document.createElement('link');
        pluginCSS.setAttribute("rel", "stylesheet");
        pluginCSS.setAttribute("type", "text/css");
        pluginCSS.setAttribute("href", "./PDFViewerPlugin.css");
        document.head.appendChild(pluginCSS);
    }

    var self = this,
        pages = [],
        pageViews = [],
        startedTextExtraction = false,
        container = null,
        initialized = false,
        pdfDocument = null,
        pageViewScroll = null,
        isPresentationMode = false,
        scale = 1,
        currentPage = 1,
        createdPageCount = 0;

    this.inPinch = false;

    //////////////////////////////////////
    //Zoom

    this.prePinch = function() {
        var i, pageView;
        renderQueue.removeIf({priority: TASK_PRIORITY.IMMEDIATE()});

        $(container).css("-webkit-overflow-scrolling", "auto");

        this.inPinch = true;
        for (i = 0; i < pageViews.length; i += 1) {
            pageView = pageViews[i];
            pageView.prePinch();
        }

    };

    this.postPinch = function(newScale) {
        var i;
        scale = newScale;

        for (i = 0; i < pageViews.length; i += 1) {
            var pageView = pageViews[i];
            //var status = getRenderingStatus(page);
            //setRenderingStatus(page, RENDERING.BLANK);
            //if (pageView.isScrolledIntoView()) {
                //pageView.scrollIn();
            pageView.postPinch();
            //}
        }
        this.inPinch = false;
        $(container).css("-webkit-overflow-scrolling", "touch");

        /*var waitRenderTasks = new jQuery.Deferred(); 
        $.when.apply($, tasks).done(function() {
            waitRenderTasks.resolve();
        });

        return waitRenderTasks.promise();*/
    };

    //Zoom
    //////////////////////////////////////
    function getDocumentProgress(progressData) {
      //self.progress(progressData.loaded / progressData.total);

        var progress = progressData.loaded / progressData.total;
        //$("#progress").width((progress * 100) + "%");
        var new_width = (progress * $('#progress').parent().width()) + 'px';
        $("#progress").animate({width: new_width}, 10);
        if (progress >= 1) {
            $("#progress").hide();
        }
    };

    this.initialize = function (viewContainer, location) {
        var self = this,
            i,
            pluginCSS;

        init(function () {
            PDFJS.workerSrc = "./pdf.viewer.worker.js";
            PDFJS.getDocument(location, null, false, getDocumentProgress).then(function loadPDF(doc) {
                pdfDocument = doc;
                container = viewContainer;

                TouchEvents.initialize({
                    container: container,
                    manager: self,
                    pinchToZoom: true
                });
                //var promiseFirstPage = new jQuery.Deferred();;
                for (i = 0; i < pdfDocument.numPages; i += 1) {
                    var promisePage = pdfDocument.getPage(i + 1);
                    promisePage.then(function(page) {
                        var pageView = new PageView({page: page, container: container, plugin: self});
                        //pageView.load(promiseFirstPage);
                        pageView.load();
                        pages.push(page);
                        pageViews.push(pageView);

                        createdPageCount += 1;
                        if (createdPageCount === (pdfDocument.numPages)) {
                            self.onLoad();
                        }
                    });
                }
                /*var promisePage = pdfDocument.getPage(1);
                    promisePage.then(function(page) {
                        var pageView = new PageView();
                        pageView.load(page, container, self);
                        pages.push(page);
                        pageViews.push(pageView);
                            self.onLoad();
                    });                
*/
                initialized = true;
            });
        });
    };

    this.isSlideshow = function () {
        // A very simple but generally true guess - if the width is greater than the height, treat it as a slideshow
        return self.getWidth() > self.getHeight();
    };

    this.onLoad = function () {};


    this.getWidth = function () {
        return pageViews[0].getWidth();
    };

    this.getHeight = function () {
        return pageViews[0].getHeight();
    };

    this.fitToWidth = function (width) {
        var zoomLevel;

        if (self.getWidth() === width) {
            return;
        }
        zoomLevel = width / self.getWidth();
        self.setZoomLevel(zoomLevel);
    };

    this.fitToHeight = function (height) {
        var zoomLevel;

        if (self.getHeight() === height) {
            return;
        }
        zoomLevel = height / self.getHeight();
        self.setZoomLevel(zoomLevel);
    };

    this.fitToPage = function (width, height) {
        var zoomLevel = width / self.getWidth();
        if (height / self.getHeight() < zoomLevel) {
            zoomLevel = height / self.getHeight();
        }
        self.setZoomLevel(zoomLevel);
    };

    this.fitSmart = function (width, height) {
        var zoomLevel = width / self.getWidth();
        if (height && (height / self.getHeight()) < zoomLevel) {
            zoomLevel = height / self.getHeight();
        }
        zoomLevel = Math.min(1.0, zoomLevel);
        self.setZoomLevel(zoomLevel);
    };

    this.setZoomLevel = function (zoomLevel) {
        var i;

        if (scale !== zoomLevel) {
            scale = zoomLevel;

            for (i = 0; i < pageViews.length; i += 1) {
                var pageView = pageViews[i];
                pageView.setScale(scale);
            }
       }
    };

    this.getZoomLevel = function () {
        return scale;
    };

    this.onScroll = function () {
        var i;

        for (i = 0; i < pageViews.length; i += 1) {
            var pageView = pageViews[i];
            pageView.scrollIn();
            /*if (pageView.isScrolledIntoView()) {
                pageView.scrollIn();
            }*/
        }
    };

    this.getPageInView = function () {
        var i;
        for (i = 0; i < pageViews.length; i += 1) {
            if (pageViews[i].isScrolledIntoView()) {
                return i + 1;
            }
        }
    };

    this.showPage = function (n) {
        var pageView = pageViews[n-1];
        pageView.scrollIntoView();
        this.onScroll();
    };

    this.getPages = function() {
        return pageViews;
    };

    this.getScale = function() {
        return scale;
    };
}
